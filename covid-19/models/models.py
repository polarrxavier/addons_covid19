# -*- coding: utf-8 -*-

import datetime
from odoo import models, fields, api


class covid_19(models.Model):
    _name = 'covid.covid19'
    source = fields.Char(string='Fuente', required=True)
    date = fields.Datetime(string='Fecha/Hora', required=True,
                            default=fields.Datetime.now(),)
    country_id = fields.Many2one(
        'res.country', string='País', required=True)
    infected = fields.Integer(string='Infectados', required=True, default=0)
    recovered = fields.Integer(string='Recuperados', required=True, default=0)
    deceaced = fields.Integer(string='Fallecidos', required=True, default=0)
    total_infected = fields.Integer(
        string='Total Infectedos', compute='set_total_infected', required=True, default=0)
    total_recovered = fields.Integer(
        string='Total Recuperados', compute='set_total_recovered', required=True, default=0)
    total_deceaced = fields.Integer(
        string='Total Fallecidos', compute='set_total_deceaced', required=True, default=0)

    def set_total_infected(self):
        for data in self:
            domain = [
                ('country_id', '=', data.country_id.id),
                ('date', '<', data.date),
                ]
            records = self.search(domain)
            Infecteds = records.mapped('infected')
            data.total_infected = sum(Infecteds) + data.infected

    def set_total_recovered(self):
        for data in self:
            domain = [
                ('country_id', '=', data.country_id.id),
                ('date', '<', data.date),
                ]
            records = self.search(domain)
            Recovered = records.mapped('recovered')
            data.total_recovered = sum(Recovered) + data.recovered

    def set_total_deceaced(self):
        for data in self:
            domain = [
                ('country_id', '=', data.country_id.id),
                ('date', '<', data.date),
                ]
            records = self.search(domain)
            Deceaced = records.mapped('deceaced')
            data.total_deceaced = sum(Deceaced) + data.deceaced

    def set_porcentage_infected(self):
        total = 0
        if self.infected:
            total = (self.infected * 100) / self.total_infected
        return total

    def set_porcentage_recovered(self):
        total = 0
        if self.recovered:
            total = (self.recovered * 100) / self.total_recovered
        return total

    def set_porcentage_deceaced(self):
        total = 0
        if self.deceaced:
            total = (self.deceaced * 100) / self.total_deceaced
        return total
